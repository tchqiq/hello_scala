import scala.collection.immutable.HashSet

/**
  * Created by heqi02 on 16/6/9.
  * 本段程序是scala学习的一个初始探测,它包括了一下内容:
  * 1.函数文本的使用(=>符号),他可以将函数作为参数,从而对集合中的一些元素做重新映射
  * 2.数组和List的使用
  * 3.val 和var的区别
  * 4.List的一些常用方法
  * 5.tuple,set,map的使用
  * 6.符号<-用于遍历,是一种左操作符
  * 7.符号:::用于添加list,是一种右操作符
  * 8.符号::用于list插入元素,是一种左操作符
  * 9.符号->表示一种映射关系,对于任何类型都可以调用->方法,此方法将返回一对键值对
  */
object HelloWorld {
  def main(args: Array[String]) {
    println("hello world")
    val name = "HelloWorld"
    val nameHasUpper = name.exists(_.isUpper)
    println(nameHasUpper)
    max(3,5)
    println(max(3,5))

    //函数文本 arg => println(arg).函数文ᴀ的语法就是，括号里的命名参数列表，右箭头，然后是函数体。
    args.foreach(arg => println(arg))
    //more sample
    args.foreach(println)

    //你可以认为<-符号代表“其中”
    for(arg<-args)
      println(arg)

    //String Array
    val greeting = new Array[String](3)
    greeting(0) = "Hello~"
    greeting(1) = "!"
    greeting(2) = "World"
    println("--------prinint greeting word")
    for (i <- 0 to 2) {
      print(greeting(i))
    }
    //Another String Array
    val numNames = Array("one","two","three")

    //use List
    val oneTwoThree = List(1, 2, 3)
    //“:::”的方法实现叠加功能,似乎这个名字指代的List看上去被改变了，而实际上它只是用新的值创建了一个List并返回。
    val oneTwo = List(1, 2)
    val threeFour = List(3, 4)
    val oneTwoThreeFour = oneTwo ::: threeFour
    println(
      """
      ------":::" be use to add ----------- """.stripMargin)
    println(oneTwo + " and " + threeFour + " were not mutated.")
    println("Thus, " + oneTwoThreeFour + " is a new List.")
    println("------use :: to insert into list---------")
    println(1:: List(2, 3))

    //Nil 的cons操作符串
    val oneTowThree = 1 :: 2 :: 3 :: Nil
    println(oneTowThree)

    println("---------------list common use api--------------")
    //list common use api
    val thrill = "Will"::"fill"::"until"::Nil
    thrill.count(s => s.length == 4)  //计算长度为 4 的 String 元素个数(返回 2
    thrill.drop(2)   //返回去掉前 2 个元素的 thrill 列表(返回 List("until"))
    thrill.dropRight(2)  //返回去掉前 2 个元素的 thrill 列表(返回 List("until"))
    thrill.exists(s => s == "until") //判断是否有值为"until"的字串元素在 thrill 里(返回 true)
    thrill.filter(s => s.length() == 4) //依次返回所有长度为 4 的元素组成的列表(返回 List("Will", "fill"))
    thrill.forall(s=> s.endsWith("l")) //辨别是否thrill列表里所有元素都以"l"结尾(返回true)
    thrill.foreach(s=> print(s))  //对 thrill 列表每个字串执行 print 语句("Willfilluntil")
    thrill.foreach(print) //与前相同,不过更简洁(同上)
    thrill.head //返回 thrill 列表的第一个元素(返回"Will")
    thrill.init //返回 thrill 列表除最后一个以外其他元素组成的列表(返回 List("Will", "fill"))
    thrill.map(s => s + "y") //返回由 thrill 列表里每一个 String 元素都加了"y"构成的列表 (返回List("Willy", "filly", "untily"))
    thrill.mkString(", ")   //用列表的元素创建字串(返回"will, fill, until")
    thrill.dropWhile(s => s.length() == 4) //返回去除了 thrill 列表中长度为 4 的元素后依次排列的元素列表 (返回 List("until"))
    println("thrill:::::" + thrill )
    println("test drop while" + thrill.dropWhile(s => s.length() == 4))
    thrill.reverse  //返回 有 thrill 列表的逆序元素的列表(返回 List("until", "fill", "Will"))
    println("sort api"+ thrill.sortWith((s,t)=>s.charAt(0).toLower<t.charAt(0).toLower)) //返回包括 thrill 列表所有元素,并且第一个字符小写按照字母顺 序排列的列表(返回List("fill", "until", "Will"))
    thrill.tail //返回除掉第一个元素的 thrill 列表(返回 List("fill", "until"))

    println("------use tuple---------")
    val pair = ('u', 'r', "the", 1, 4, "me")
    println(pair._1)  //note this is begin from 1
    print(pair._2)
    println(pair._5)

    println("------use set---------")
    var hashSet = HashSet("Tomatoes", "Chilies")
    hashSet += "AB"
    println(hashSet )

    import scala.collection.mutable.Set
    val movieSet = Set("Hitch", "Poltergeist")
    movieSet += "Shark"
    movieSet.+=("Shark")
    println("use Set " + movieSet)

    import  scala.collection.immutable.Set
    val hashSet2 = HashSet("Tomatoes", "Chilies")
    println(hashSet2 + "Coriander")


      println ("------use map---------")
    var treasureMap = Map[Int, String]()
    treasureMap += (1->"Go to Island")
    treasureMap += (2->"Find big x on grand")
    treasureMap += (3->"dig")
    //这个->方法可以调用Scala程序里的任何对象，并返回一个包৿键和值的二元元组
    println(treasureMap(2))
    val romanNumeral = Map(
      1 -> "I",2 -> "II",3 -> "III",4 -> "IV",5 -> "V",6 -> "VI"
    )
    println(romanNumeral(4))

    println("------function style---------")
    val res = formatArgs(Array("zero", "one", "two"))
    assert(res == "zero\none\ntwo")

    //io for read file
    def countchars4(args:Array[String]): Unit = {
      import scala.io.Source
      def widthOfLength(s:String) = s.length.toString.length

      if(args.length>0) {
        val lines = Source.fromFile(args(0)).getLines().toList
        val longestLine = lines.reduceLeft(
          (a, b) => if (a.length>b.length) a else b
        )
        val maxWidth = widthOfLength(longestLine)

        for (line <- lines) {
          val numSpaces = maxWidth - widthOfLength(line)
          val padding = " " * numSpaces

          print(padding + line.length +" | "+ line)
        }

      }
      else
        Console.err.println("Please enter filename")
    }


  }


  //function method
  def printArgs(args: Array[String]): Unit = {
    var i = 0
    while (i < args.length) {
      println(args(i))
      i += 1 }
  }
  //more fuction style
  def printArgs2(args: Array[String]): Unit = {
    for (arg <- args) {
      println(arg)
    }
  }
  //or like this
  def printArg3(args: Array[String]): Unit = {
    args.foreach(println)
  }
  /**
    * 当然,你可以走得更远。重构后的 printArgs 方法并不是纯函数式的,因为它有副作用
    * 其副作用是打印到标准输出流。函数有副作用的马脚就是结果类型为 Unit。
    * 如果某个函数不返回任何有用的值,就是说其结果类型为Unit,那么那个函数唯一能让世界 有点儿变化的办法就是通过某种副作用。
    * 更函数式的方式应该是定义对需打印的 arg 进行 格式化的方法,但是仅返回格式化之后的字串
    * @param args
    * @return String
    */
  def formatArgs(args: Array[String]) = args.mkString("\n")

  def max(x:Int,y:Int):Int = {
    if(x>y) {
      x
    }
    else y
  }
  def max2(x: Int, y: Int) = if (x > y) x else y

}
