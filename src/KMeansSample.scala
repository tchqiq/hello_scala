import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.{SparkContext, SparkConf, ml}

/**
  * 该程序主要通过kmeans算法对数据进行分类
  * 执行方式：./spark-submit --master=spark://cloud25:7077 --class com.eric.spark.mllib.KMeansSample --executor-memory=2g /opt/cloud/spark-1.4.1-bin-hadoop2.6/lib/spark_scala.jar
  * Created by heqi02 on 16/9/14.
  */
object KMeansSample {
  def main(args: Array[String]) {

    val conf = new SparkConf().setAppName("KMeansSample");
    val sc = new SparkContext(conf)


    val fileData=sc.textFile("/data/mllib/kmeans_data.txt",1)
    val parseData=fileData.map(record=>Vectors.dense(record.split(",").map(_.toDouble)))
    //creates a DataFrame
    val dataset = Seq(
      (1, Vectors.dense(0.0, 0.0, 0.0)),
      (2, Vectors.dense(0.1, 0.1, 0.1)),
      (3, Vectors.dense(0.2, 0.2, 0.2)),
      (4, Vectors.dense(9.0, 9.0, 9.0)),
      (5, Vectors.dense(9.1, 9.1, 9.1)),
      (6, Vectors.dense(9.2, 9.2, 9.2))
    )
    //  val dataset2 = Vectors.dense(0.0, 0.0, 0.0) ::: Vectors.dense(0.0, 0.0, 0.1)

    // Trains a k-means model.
    //模型训练
    val dataModelNumber=2;
    val dataModelTrainTimes=20
    val model=KMeans.train(parseData,dataModelNumber,dataModelTrainTimes)


    // Shows the result.
    println("Cluster Centers: ")
    model.clusterCenters.foreach(println)

    //使用模型测试单点数据
    //运行结果
    //    Vectors 0.2 0.2 0.2 is belongs to clusters:0
    //    Vectors 0.25 0.25 0.25 is belongs to clusters:0
    //    Vectors 8 8 8 is belongs to clusters:1
    println("Vectors 0.2 0.2 0.2 is belongs to clusters:" + model.predict(Vectors.dense("0.2 0.2 0.2".split(' ').map(_.toDouble))))
    println("Vectors 0.25 0.25 0.25 is belongs to clusters:" + model.predict(Vectors.dense("0.25 0.25 0.25".split(' ').map(_.toDouble))))
    println("Vectors 8 8 8 is belongs to clusters:" + model.predict(Vectors.dense("8 8 8".split(' ').map(_.toDouble))))

    //交叉评估1，只返回结果
    val testdata = fileData.map(s =>Vectors.dense(s.split(',').map(_.toDouble)))
    val result1 = model.predict(testdata)
    result1.saveAsTextFile("/data/mllib/output/kmeans/result1")

    //交叉评估2，返回数据集和结果
    val result2 = fileData.map {
      line =>
        val linevectore = Vectors.dense(line.split(',').map(_.toDouble))
        val prediction = model.predict(linevectore)
        line + " " + prediction
    }.saveAsTextFile("/data/mllib/output/kmeans/result2")

    sc.stop()

  }

}
