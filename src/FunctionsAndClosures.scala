/**
  * Created by heqi02 on 2016/11/15.
  */
object FunctionsAndClosures {

  //8.2 Local functions
  object LongLines {
    def processFile(filename: String, width: Int): Unit = {
      import scala.io.Source
      object LongLines {
        def processLine(line: String): Unit = {
          if (line.length > width)
            print(filename + ": " + line)
        }

        val source = Source.fromFile(filename).getLines()

        for (line <- source)
          processLine(line)

      }
    }
  }

  object FindLongLines {
    def main(args: Array[String]) {
      val width = args(0).toInt
      for (arg <- args.drop(1)) {
        LongLines.processFile(arg, width)
      }
    }
  }

  //8.6 Partially applied functions
  def misc1() {
    val someNumbers = List(-11, -10, -5, 0, 5, 10)

    someNumbers.foreach(println _)

    someNumbers.foreach(x => println(x))

    someNumbers.foreach(println _)

    someNumbers.foreach(println)

  }

  //8.9 Tail recursion
  def isGoodEnough(guess: Double): Boolean = {
    println("guess [" + guess + "]")
    Math.abs(guess * guess - 2.0) < 1.0E-6
  }

  def improve(guess: Double): Double =
    (guess + 2.0 / guess) / 2.0

  def approximate(guess: Double): Double = {
    if (isGoodEnough(guess)) guess
    else approximate(improve(guess))
  }

  def approximateLoop(initialGuess: Double): Double = {
    var guess = initialGuess
    while (!isGoodEnough(guess))
      guess = improve(guess)
    guess
  }


  def boom(x: Int): Int = {
    if (x == 0) throw new Exception("boom!")
    else boom(x - 1) + 1 //结尾+1了,不是尾递归
  }

  def bang(x: Int): Int =
    if (x == 0) throw new Exception("bang!")
    else bang(x - 1) //尾递归


  //以下没有尾调用优化
  def isEven(x: Int): Boolean =
    if (x == 0) true else isOdd(x - 1)

  def isOdd(x: Int): Boolean =
    if (x == 0) false else isEven(x - 1)

  val funValue = nestedFun _
  def nestedFun(x: Int): Int = {
    if (x != 0) {
      println(x);
      funValue(x - 1)
    } else x
  }

  //  val funValue = nestedFun _
  //  def nestedFun(x: Int) { //这种写法默认返回的函数返回值必须是unit
  //    if (x != 0) { println(x); funValue(x - 1) }
  //  }

  def main(args: Array[String]) {
    misc1()

    println("gcbx isEven(2) [" + isEven(2) + "]")
    println("gcbx isEven(3) [" + isEven(3) + "]")
    nestedFun(2)

    println("===========8.9 Tail recursion=========")
    approximate(3.3)

    boom(3)

  }

}
