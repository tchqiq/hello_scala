/**
  * Created by heqi02 on 16/6/18.
  */
object ClassesAndObjects {
  /**
    * A class that calculates a checksum of bytes. This class
    * is not thread-safe.
    */
  class ChecksumAccumulator{
    private var sum = 0
    /**
      * Adds the passed <code>Byte</code> to the checksum
      * calculation.
      *
      * $@$param b the <code>Byte</code> to add
      */
    def add (b:Byte) {sum+=b}

    /**
      * Gets a checksum for all the <code>Byte</code>s passed
      * to <code>add</code>. The sum of the integer
      * returned by this method, when added to the
      * sum of all the passed bytes will yield zero.
      */
    def checksum():Int = ~(sum & 0XFF) +1
  }

  object ChecksumAccumulator{
    import scala.collection.mutable.Map
    private val cache = Map[String, Int]()
    def calculate (s : String):Int = {
      if (cache.contains(s)) {
        cache(s)
      } else {
        val acc = new ChecksumAccumulator
        for (c <- s) {
          acc.add(c.toByte)
        }
        val cs = acc.checksum()
        cache += (s -> cs)
        cs
      }
    }
  }

  def main(args: Array[String]) {
    val sum = ChecksumAccumulator.calculate("hello")
    val sum2 = ChecksumAccumulator.calculate("hello")
    println("sum [" + sum + "]")
    println("sum2 [" + sum2 + "]")
  }

}
