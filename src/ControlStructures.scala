import java.net.{MalformedURLException, URL}

/**
  * Created by heqi02 on 2016/11/9.
  */
object ControlStructures {


  val filesHere = (new java.io.File("./src")).listFiles


  def printFiles() {
    for (file <- filesHere)
      println(file)
  }

  def printFilesIter() {
    // Not common in Scala...
    for (i <- 0 to filesHere.length - 1)
      println(filesHere(i))
  }

  def printScalaFiles() {

    for (file <- filesHere if file.getName.endsWith(".scala"))
      println(file)
  }

  def printScalaFiles2() {
    for (file <- filesHere)
      if (file.getName.endsWith(".scala"))
        println(file)
  }

  def printScalaFiles3() {
    for (
      file <- filesHere
      if file.isFile;
      if file.getName.endsWith(".scala")
    ) println(file)
  }

  def fileLines(file: java.io.File) = scala.io.Source.fromFile(file).getLines().toList

  //  def grepParens(pattern: String) {
  //    def grep(pattern: String) = {
  //      for (
  //        file <- filesHere
  //        if file.getName.endsWith(".scala");
  //        line <- fileLines(file)
  //        if line.trim.matches(pattern);
  //      ) println(file +": "+ line.trim)
  //    }
  //    grep(pattern)
  //  }

  def grepParens(pattern: String) {
    def grep(pattern: String) =
      for (
        file <- filesHere
        if file.getName.endsWith(".scala");
        line <- fileLines(file)
        if line.trim.matches(pattern)
      ) println(file + ": " + line.trim)

    grep(pattern)
  }


  def grepGcd() {
    def grep(pattern: String) = grepParens(pattern)
    grep(".*gcd.*")
  }

  //使用大括号的一个好处是你可以省略一些使用小括号必须加的分号
  def grepGcd2() {
    def grep(pattern: String) =
      for {
        file <- filesHere
        if file.getName.endsWith(".scala")
        line <- fileLines(file)
        trimmed = line.trim
        if trimmed.matches(pattern)
      } println(file + ": " + trimmed)

    grep(".*gcd.*")
  }

  def scalaFiles = {
    for {
      file <- filesHere
      if file.getName.endsWith(".scala")
    } yield file
  }


  val forLineLengths = {
    for {
      file <- filesHere
      if file.getName.endsWith(".scala")
      line <- fileLines(file)
      trimmed = line.trim
      if trimmed.matches(".*for.*")
    } yield trimmed.length
  }

  //7.4 Exception
  def urlFor(path: String): Unit = {
    try {
      new URL(path)
    } catch {
      case e: MalformedURLException =>
        new URL("http://www.scala-lang.org")
    }
  }

  //7.5 Match expressions
  def match1(args: Array[String]) = {
    val firstArgs = if (args.length > 0) args(0) else ""
    firstArgs match {
      case "salt" => println("pepper")
      case "chips" => println("salsa")
      case "eggs" => println("bacon")
      case _ => println("huh?")
    }
  }

  def match2(args: Array[String]) = {
    val firstArgs = if (args.length > 0) args(0) else ""
    val friend =
      firstArgs match {
        case "salt" => "pepper"
        case "chips" => "salsa"
        case "eggs" => "bacon"
        case _ => "huh?"
      }
    println(friend)
  }

  //7.7 Variable scope
  def printMultiTable(): Unit = {
    var i = 1
    // only i in scope here
    while (i <= 10) {
      var j = 1
      // both i and j in scope here
      while (j <= 10) {
        val prod = i * j
        var k = prod.toString.length

        while (k < 4) {
          print(" ")
          k += 1
        }
        print(prod)
        j += 1
      }
      // i and j still in scope; prod and k out of scope

      println()
      i += 1

    }
    // i still in scope; j, prod, and k out of scope
  }

  //7.8 Refactoring imperative-style code
  // Returns a row as a sequence
  def makeRowSeq(row: Int) ={
    for(col <- 1 to 10) yield {
      val prod = (row * col).toString
      val padding = " " * (4 - prod.length)
      padding + prod
    }
  }
  // Returns a row as a string
  def makeRow(row: Int) = makeRowSeq(row).mkString
  // Returns table as a string with one row per line
  def multiTable() = {
    val tableSeq = for (row <- 1 to 10) yield {
      makeRow(row)
    }
    tableSeq.mkString("\n")

  }

    def main(args: Array[String]) {
    ControlStructures.printFiles()
    ControlStructures.printFilesIter()
    ControlStructures.printScalaFiles()
    ControlStructures.printScalaFiles2()
    ControlStructures.printScalaFiles3()
    println("Files.scalaFiles.toList [" + ControlStructures.scalaFiles.toList + "]")
    println("Files.forLineLengths.toList [" + ControlStructures.forLineLengths.toList + "]")

    println("==========test match===========")
    match1(Array("foo"))
    match1(Array("salt"))
    match2(Array("chips"))

    println("=============print multi table =============")
    printMultiTable()

    println("=============print multi table2 =============")
      print(multiTable())
  }

}
