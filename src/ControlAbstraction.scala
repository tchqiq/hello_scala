/**
  * Created by heqi02 on 2016/11/27.
  */
object ControlAbstraction {
  private def filesHere = (new java.io.File("./src")).listFiles()

  def filesEnding(query: String) =
    for (file <- filesHere; if file.getName.endsWith(query))
      yield file

  def filesContaining(query: String) =
    for (file <- filesHere; if file.getName.contains(query))
      yield file

  def filesRegex(query: String) =
    for (file <- filesHere; if file.getName.matches(query))
      yield file

  //9.1 Reducing code duplication above code
  def filesMatching(query: String, matcher: (String, String) => Boolean) =
    for (file <- filesHere; if matcher(file.getName, query))
      yield file


  //9.1 more clean
  def filesEnding2(query: String) =
    filesMatching(query, _.endsWith(_))

  def fileContaining2(query: String) =
    filesMatching(query, _.contains(_))

  def filesRegex2(query: String) =
    filesMatching(query, _.matches(_))

  //9.1 more and more clean
  def filesMatching2(query: String, matcher: String => Boolean) =
    for (file <- filesHere; if matcher(file.getName))
      yield file

  def filesEnding3(query: String) =
    filesMatching2(query, _.endsWith(query))

  def filesContaining3(query: String) =
    filesMatching2(query, _.contains(query))


  def filesRegex3(query: String) =
    filesMatching2(query, _.matches(query))

  def main(args: Array[String]) {
    println("FileMatcher.filesEnding(\"scala\").toList [" +
      filesEnding("scala").toList + "]")
    println("FileMatcher.filesContaining(\"Files1\").toList [" +
      filesContaining("Files1").toList + "]")
    println("FileMatcher.filesRegex(\".*Re.ex.*\").toList [" +
      filesRegex(".*Re.ex.*").toList + "]")

    println("=================more clean =================")
    println("FileMatcher.filesEnding(\"scala\").toList [" +
      filesEnding3("scala").toList + "]")
    println("FileMatcher.filesContaining(\"Files1\").toList [" +
      filesContaining3("Files1").toList + "]")
    println("FileMatcher.filesRegex(\".*Re.ex.*\").toList [" +
      filesRegex3(".*Re.ex.*").toList + "]")
  }
}
