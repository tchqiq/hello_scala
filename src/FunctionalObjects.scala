/**
  * Created by heqi02 on 2016/11/7.
  * No.6 chapter
  */
object FunctionalObjects {

  class Rational(n: Int, d: Int) {
    require(d != 0)


    private val g = gcd(n.abs, d.abs)
    val numer: Int = n / g
    val denom: Int = d / g

    override def toString = numer + "/" + denom

    def add(that: Rational): Rational =
      new Rational(
        numer * that.denom + that.numer * denom,
        denom * that.denom
      )

    //6.6 Self references
    def lessThan(that: Rational) = this.numer * that.denom < that.numer * this.denom

    //不可省略的this
    def max(that: Rational) = if (this.lessThan(that)) that else this

    //6.7 Auxiliary constructors 从构造函数
    def this(n: Int) = this(n, 1) // auxiliary constructor

    //6.9 Defining operators
    def +(that: Rational): Rational = {
      new Rational(numer * that.denom + that.numer * denom, denom * that.denom)
    }

    //6.11 Method overloading
    def +(that: Int): Rational = {
      new Rational(numer + that * denom, denom)
    }

    def -(that: Rational): Rational = {
      new Rational(numer * that.denom - that.numer * denom, denom * that.denom)
    }

    def -(that: Int): Rational = {
      new Rational(numer - that * denom, denom)
    }

    def *(that: Rational): Rational = {
      new Rational(numer * that.numer, denom * that.denom)
    }

    def *(that: Int): Rational = {
      new Rational(numer * that, denom)
    }

    def /(that: Rational): Rational = {
      new Rational(numer * that.denom, denom * that.numer)
    }

    def /(that: Int): Rational = {
      new Rational(numer, denom * that)
    }

    //6.8 Private fields and methods
    private def gcd(a: Int, b: Int): Int = {
      if (b == 0) a else gcd(b, a % b)
    }

  }


  def main(args: Array[String]) {
    val oneHalf = new Rational(1, 2)
    val twoThirds = new Rational(2, 3)
    val sum = oneHalf add twoThirds

    println("oneHalf [" + oneHalf + "]")
    println("twoThirds [" + twoThirds + "]")
    println("sum [" + sum + "]")
    println("sum.numer [" + sum.numer + "]")
    println("sum.denom [" + sum.denom + "]")

    //6.8 Private fields and methods
    println("new Rational(66, 42) [" + new Rational(66, 42) + "]")
    println("new Rational(4, 6) [" + new Rational(4, 6) + "]")

    //6.9 defind operator
    val x = new Rational(1, 2)
    val y = new Rational(2, 3)
    println("x [" + x + "]")
    println("y [" + y + "]")
    println("x + y [" + (x + y) + "]")
    println("x.+(y) [" + (x.+(y)) + "]")
    println("x + x * y [" + (x + x * y) + "]")
    println("(x + x) * y [" + ((x + x) * y) + "]")
    println("x + (x * y) [" + (x + (x * y)) + "]")

    println("---------------6.10 method overlaoding-----------")
    println("x [" + x + "]")
    println("x * x [" + (x * x) + "]")
    println("x * 2 [" + (x * 2) + "]")

    implicit def intToRational(x: Int) = new Rational(x)
    val r = new Rational(2,3)
    println("2 * r [" + (2 * r) + "]")
  }

}
